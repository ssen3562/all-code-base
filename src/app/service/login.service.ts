import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public loginAuthenticalted = new BehaviorSubject<string>('Not Authenticated');
  constructor() { }

  setStatus(value: string) {
    this.loginAuthenticalted.next(value); //it is publishing this value to all the subscribers that have already subscribed to this message
  }
}
