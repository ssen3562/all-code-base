import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showModal:boolean = false
  model: any = {};wrongData:boolean;
  isLoggedIn:boolean=false;
  constructor(public login:LoginService) { }

  onSubmit() {
    if(this.model.firstName === 'admin' && this.model.password==='admin'){
      this.login.setStatus('Authenticated');
      this.isLoggedIn = true;
      this.showModalFunc();
    }else{
       this.login.setStatus('Not Authenticated');
       this.wrongData= true;
       this.isLoggedIn = false;
    }
  }
  logout(){
    this.login.setStatus('Not Authenticated');
    this.isLoggedIn = false;
  }
  showModalFunc(){
    this.showModal = !this.showModal;
  }
  ngOnInit() {
  }

}
