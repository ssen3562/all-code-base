import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  authication:boolean;
  constructor(public login:LoginService) { }

  ngOnInit() {
    this.login.loginAuthenticalted.subscribe((data)=>{
      this.authication = data === 'Authenticated'? true: false; 
    })
  }

}
